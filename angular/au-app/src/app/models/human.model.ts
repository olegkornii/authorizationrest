import {Role} from './role.model';

export class Human {

  id: string;
  name: string;
  username: string;
  email: string;
  password: string;
  roles: Role[];
}
