package com.restauthorization.restauthorization.security.jwt;

import com.restauthorization.restauthorization.services.UserPrinciple;
import io.jsonwebtoken.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * generate a JWT token
 * valiate a JWT token
 * parse username from JWT token
 */
@Component
public class JwtProvider {

    private static final org.apache.log4j.Logger dbLogger = org.apache.log4j.Logger.getLogger(JwtProvider.class);

    //@Value("${com.restAu.jwtSecret}")
    private String jwtSecret = "jwtGrokonezSecretKey";

    // @Value("${com.restAu.jwtExpiration}")
    private int jwtExpiration = 86400;


    /**
     * @param authentication gets username
     * @return JWT Token with username, Date() object, secretKey
     */
    public String generateJwtToken(Authentication authentication) {

        UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Validates Token
     *
     * @param authToken
     * @return true or false
     */
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            dbLogger.error("Invalid JWT signature -> Message: {} ", e);
        } catch (MalformedJwtException e) {
            dbLogger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            dbLogger.error("Expired JWT token -> Message: {}", e);
        } catch (UnsupportedJwtException e) {
            dbLogger.error("Unsupported JWT token -> Message: {}", e);
        } catch (IllegalArgumentException e) {
            dbLogger.error("JWT claims string is empty -> Message: {}", e);
        }

        return false;
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }
}
