package com.restauthorization.restauthorization.messages.request;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class SignUpFormTest {
    private SignUpForm signUpForm = new SignUpForm();

    @Test
    public void getName() {
        signUpForm.setName("Oleg");
        assertEquals("Oleg", signUpForm.getName());
    }

    @Test
    public void setName() {
        signUpForm.setName("New");
        assertEquals("New", signUpForm.getName());
    }

    @Test
    public void getUsername() {
        signUpForm.setUsername("Oleg");
        assertEquals("Oleg", signUpForm.getUsername());
    }

    @Test
    public void setUsername() {
        signUpForm.setUsername("new");
        assertEquals("new", signUpForm.getUsername());
    }

    @Test
    public void getEmail() {
        signUpForm.setEmail("oleg@gmail.com");
        assertEquals("oleg@gmail.com", signUpForm.getEmail());
    }

    @Test
    public void setEmail() {
        signUpForm.setEmail("new@gmail.com");
        assertEquals("new@gmail.com", signUpForm.getEmail());
    }

    @Test
    public void getPassword() {
        signUpForm.setPassword("1234");
        assertEquals("1234", signUpForm.getPassword());
    }

    @Test
    public void setPassword() {
        signUpForm.setPassword("new");
        assertEquals("new", signUpForm.getPassword());
    }

    @Test
    public void getRole() {
        Set<String> roles = new HashSet<>();
        roles.add("Admin");
        roles.add("User");
        signUpForm.setRole(roles);
        assertEquals(roles, signUpForm.getRole());
    }

    @Test
    public void setRole() {
        Set<String> roles = new HashSet<>();
        roles.add("User");
        signUpForm.setRole(roles);
        assertEquals(roles, signUpForm.getRole());
    }
}