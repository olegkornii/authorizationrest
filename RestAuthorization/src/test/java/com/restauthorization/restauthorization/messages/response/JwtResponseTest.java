package com.restauthorization.restauthorization.messages.response;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import static org.mockito.Mockito.*;

import java.util.Collection;

import static org.junit.Assert.*;

public class JwtResponseTest {
    private Collection<? extends GrantedAuthority> authorities = mock(Collection.class);
    private JwtResponse jwtResponse = new JwtResponse("token", "username", authorities);

    @Test
    public void getAccessToken() {
        assertEquals("token", jwtResponse.getAccessToken());
    }

    @Test
    public void setAccessToken() {
        jwtResponse.setAccessToken("newToken");
        assertEquals("newToken", jwtResponse.getAccessToken());
    }

    @Test
    public void getTokenType() {
        jwtResponse.setTokenType("TokenType");
        assertEquals("TokenType", jwtResponse.getTokenType());
    }

    @Test
    public void setTokenType() {
        jwtResponse.setTokenType("NewType");
        assertEquals("NewType", jwtResponse.getTokenType());
    }

    @Test
    public void getUsername() {
        assertEquals("username", jwtResponse.getUsername());
    }

    @Test
    public void setUsername() {
        jwtResponse.setUsername("NewName");
        assertEquals("NewName", jwtResponse.getUsername());
    }

    @Test
    public void getAuthorities() {
        assertEquals(authorities, jwtResponse.getAuthorities());
    }
}