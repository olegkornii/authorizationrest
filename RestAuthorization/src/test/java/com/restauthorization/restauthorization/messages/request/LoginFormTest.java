package com.restauthorization.restauthorization.messages.request;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoginFormTest {

    private LoginForm loginForm = new LoginForm();

    @Test
    public void getUsername() {
        loginForm.setUsername("OlegKornii");
        assertEquals("OlegKornii", loginForm.getUsername());
    }

    @Test
    public void setUsername() {
        loginForm.setUsername("New");
        assertEquals("New", loginForm.getUsername());
    }

    @Test
    public void getPassword() {
        loginForm.setPassword("1234");
        assertEquals("1234", loginForm.getPassword());
    }

    @Test
    public void setPassword() {
        loginForm.setPassword("new");
        assertEquals("new", loginForm.getPassword());
    }
}