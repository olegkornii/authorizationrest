package com.restauthorization.restauthorization.messages.response;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResponseMessageTest {
    private ResponseMessage responseMessage = new ResponseMessage("hello");

    @Test
    public void getMessage() {

        assertEquals("hello", responseMessage.getMessage());
    }

    @Test
    public void setMessage() {
        responseMessage.setMessage("new");
        assertEquals("new", responseMessage.getMessage());
    }
}