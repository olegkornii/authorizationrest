package com.restauthorization.restauthorization.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class RoleTest {
    private Role role = new Role();
    RoleName roleName = RoleName.ROLE_USER;

    @Test
    public void getId() {
        role.setId(10L);
        assertEquals(10L, (long) role.getId());
    }

    @Test
    public void setId() {
        role.setId(15L);
        assertEquals(15L, (long) role.getId());
    }

    @Test
    public void getName() {
        role.setName(roleName);
        assertEquals(roleName, role.getName());
    }

    @Test
    public void setName() {
        RoleName newName = RoleName.ROLE_ADMIN;
        role.setName(newName);
        assertEquals(newName, role.getName());
    }

    @Test
    public void constructorTest() {
        RoleName newName = RoleName.ROLE_ADMIN;
        Role role = new Role(newName);
        assertEquals(newName, role.getName());
    }
}