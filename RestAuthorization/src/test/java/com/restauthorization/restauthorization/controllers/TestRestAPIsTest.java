package com.restauthorization.restauthorization.controllers;

import com.restauthorization.restauthorization.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class TestRestAPIsTest {
    @InjectMocks
    TestRestAPIs testRestAPIs;

    @Mock
    UserRepository userRepository;

    @Before
    public void set_up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void userAccess() {
        assertNotNull(testRestAPIs.userAccess());
        assertEquals(">>> User Contents!", testRestAPIs.userAccess());
    }

    @Test
    public void projectManagementAccess() {
        assertNotNull(testRestAPIs.projectManagementAccess());
        assertEquals(">>> Project Management Board", testRestAPIs.projectManagementAccess());
    }
}