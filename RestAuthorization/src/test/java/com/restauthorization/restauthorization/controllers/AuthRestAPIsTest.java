package com.restauthorization.restauthorization.controllers;

import com.restauthorization.restauthorization.messages.request.LoginForm;
import com.restauthorization.restauthorization.messages.request.SignUpForm;
import com.restauthorization.restauthorization.model.Role;
import com.restauthorization.restauthorization.model.RoleName;
import com.restauthorization.restauthorization.repository.RoleRepository;
import com.restauthorization.restauthorization.repository.UserRepository;
import com.restauthorization.restauthorization.security.jwt.JwtProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class AuthRestAPIsTest {

    @InjectMocks
    AuthRestAPIs authRestAPIs;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private JwtProvider jwtProvider;

    LoginForm loginForm;
    SignUpForm signUpForm = new SignUpForm();

    @Before
    public void set_up() {
        MockitoAnnotations.initMocks(this);

        Set<String> roles = new HashSet<>();
        roles.add("admin");
        roles.add("pm");
        roles.add("user");

        loginForm = new LoginForm();
        loginForm.setPassword("1234");
        loginForm.setUsername("OlegKornii");

        signUpForm.setUsername("OlegKornii");
        signUpForm.setEmail("oleg@gmail.com");
        signUpForm.setPassword("12345678");
        signUpForm.setName("Oleg");
        signUpForm.setRole(roles);

    }

    @Test
    public void authenticateUser() {

    }

    @Test
    public void registerUser() {
        RoleName admin = RoleName.ROLE_ADMIN;
        RoleName pm = RoleName.ROLE_PM;
        RoleName user = RoleName.ROLE_USER;

        Role aRole = new Role(admin);
        Role pRole = new Role(pm);
        Role uRole = new Role(user);

        Optional<Role> aRoleV = Optional.of(aRole);
        Optional<Role> pRoleV = Optional.of(pRole);
        Optional<Role> uRoleV = Optional.of(uRole);

        when(userRepository.existsByUsername(anyString())).thenReturn(false);
        when(userRepository.existsByEmail(anyString())).thenReturn(false);
        when(roleRepository.findByName(RoleName.ROLE_ADMIN)).thenReturn(aRoleV);
        when(roleRepository.findByName(RoleName.ROLE_PM)).thenReturn(pRoleV);
        when(roleRepository.findByName(RoleName.ROLE_USER)).thenReturn(uRoleV);

        assertNotNull(authRestAPIs.registerUser(signUpForm));


    }

    @Test
    public void registerUser_existByUserName() {
        when(userRepository.existsByUsername(anyString())).thenReturn(true);

        assertNotNull(authRestAPIs.registerUser(signUpForm));
    }

    @Test
    public void registerUser_existByEmail() {
        when(userRepository.existsByUsername(anyString())).thenReturn(false);
        when(userRepository.existsByEmail(anyString())).thenReturn(true);

        assertNotNull(authRestAPIs.registerUser(signUpForm));
    }

}