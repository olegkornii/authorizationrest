package com.restauthorization.restauthorization.services;

import com.restauthorization.restauthorization.model.User;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class UserPrincipleTest {
    List<GrantedAuthority> authorities = mock(List.class);
    UserPrinciple userPrinciple;
    UserPrinciple userPrinciple2 = new UserPrinciple(1L,
            "Oleg",
            "OlegKornii",
            "oleg@gmail.com",
            "12345678",
            authorities);
    User user = new User("Oleg", "OlegKornii", "oleg@gmail.com", "12345678");

    @Test
    public void build() {
        user.setId(1L);
        userPrinciple = UserPrinciple.build(user);
        assertEquals(userPrinciple2, userPrinciple);
    }


    @Test
    public void getId() {
        assertEquals(1L, (long) userPrinciple2.getId());
    }

    @Test
    public void getName() {
        assertEquals("Oleg", userPrinciple2.getName());
    }

    @Test
    public void getEmail() {
        assertEquals("oleg@gmail.com", userPrinciple2.getEmail());
    }

    @Test
    public void getUsername() {
        assertEquals("OlegKornii", userPrinciple2.getUsername());
    }

    @Test
    public void getPassword() {
        assertEquals("12345678", userPrinciple2.getPassword());
    }

    @Test
    public void getAuthorities() {
        assertEquals(authorities, userPrinciple2.getAuthorities());
    }

    @Test
    public void isAccountNonExpired() {
        assertTrue(userPrinciple2.isAccountNonExpired());
    }

    @Test
    public void isAccountNonLocked() {
        assertTrue(userPrinciple2.isAccountNonLocked());
    }

    @Test
    public void isCredentialsNonExpired() {
        assertTrue(userPrinciple2.isCredentialsNonExpired());
    }

    @Test
    public void isEnabled() {
        assertTrue(userPrinciple2.isEnabled());
    }

    @Test
    public void equals() {
        user.setId(1L);
        userPrinciple = new UserPrinciple(user.getId(),
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities);
        assertTrue(userPrinciple2.equals(userPrinciple));

        assertFalse(userPrinciple2.equals(new UserPrinciple(10L,
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities)));
        assertTrue(userPrinciple2.equals(new UserPrinciple(user.getId(),
                "AnotherName",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities)));
        assertFalse(userPrinciple2.equals(null));

    }

    @Test
    public void HashTest() {
        userPrinciple = new UserPrinciple(user.getId(),
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities);
        UserPrinciple userPrinciple3 = userPrinciple2;
        assertFalse(userPrinciple2.hashCode() == userPrinciple.hashCode());
        assertTrue(userPrinciple2.hashCode() == userPrinciple3.hashCode());
    }
}